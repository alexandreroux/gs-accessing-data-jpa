package hello;

public interface CustomerService {

    void save(Customer customer);

    Customer findOne(Long id);
}
