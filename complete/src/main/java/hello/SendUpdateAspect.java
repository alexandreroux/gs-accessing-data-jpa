package hello;

import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class SendUpdateAspect {

    private static final Logger log = LoggerFactory.getLogger(SendUpdateAspect.class);

    @AfterReturning("execution(* *(..)) && @annotation(hello.SendUpdateEvent)")
    public void onUpdate() {
        log.info("AfterReturning aspect code");
    }
}
