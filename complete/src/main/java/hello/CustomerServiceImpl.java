package hello;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.support.AopUtils;
import org.springframework.beans.BeansException;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionSynchronizationAdapter;
import org.springframework.transaction.support.TransactionSynchronizationManager;

@Service
@CacheConfig(cacheNames = "customer")
public class CustomerServiceImpl implements CustomerService, ApplicationContextAware{

    private static final Logger log = LoggerFactory.getLogger(CustomerServiceImpl.class);

    private final CustomerRepository customerRepository;

    private CustomerServiceImpl selfProxied;

    public CustomerServiceImpl(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public void save(Customer customer) {
        selfProxied.innerSave(customer);
        log.info("afterCompletion code");
//        TransactionSynchronizationManager.registerSynchronization(new TransactionSynchronizationAdapter(){
//
//            @Override
//            public void afterCompletion(int i){
//                log.info("afterCompletion code");
//            }
//
//            @Override
//            public int getOrder() {
//                return Ordered.HIGHEST_PRECEDENCE;
//            }
//        });
//        TransactionSynchronizationManager.registerSynchronization(new TransactionSynchronizationAdapter(){
//
//            @Override
//            public void afterCompletion(int i){
//                log.info("AFTER COMMIT2");
//            }
//
//            @Override
//            public int getOrder() {
//                return Ordered.LOWEST_PRECEDENCE;
//            }
//        });
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @CacheEvict(key = "#customer.id")
    public void innerSave(Customer customer) {
        customerRepository.save(customer);
    }

    @Override
    @Cacheable
    public Customer findOne(Long id) {
        return customerRepository.findOne(id);
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        selfProxied = applicationContext.getBean(CustomerServiceImpl.class);
    }
}
