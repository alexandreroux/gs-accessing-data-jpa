package hello;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;

@SpringBootApplication
public class Application {

	private static final Logger log = LoggerFactory.getLogger(Application.class);

	public static void main(String[] args) {
		SpringApplication.run(Application.class);
	}

	@Bean
	public CommandLineRunner demo(CustomerRepository repository, CustomerServiceImpl customerService) {
		return (args) -> {
			customerService.save(new Customer("Jack", "Bauer"));

			Customer customer = customerService.findOne(1L);
			log.info("1. CUSTOMER -> " + customer.toString());

			customerService.save(new Customer(1L, "Claude", "Dupont"));

			log.info("1.1 AFTER SAVE");
			Customer customer2 = customerService.findOne(1L);
			log.info("2. CUSTOMER -> " + customer2.toString());
		};
	}

}
