Basé sur l'exemple Spring Boot JPA.

Le code custom est dans `complete`

L'idée s'est de réussir à exécuter du code après que `CustomerService.save()` ait été a appelé, càd après que la transaction soit commitée et que le cache ait été `evicted`, et ceci sans wrapper `CustomerService.save()` dans un autre service. 

J'ai essayé deux approches pour tester :

* Rajouter `Transaction...afterCompletion` dans la définition du `CustomerService.save()`, mais je n'arrive pas à le faire s'exécuter après l'éviction du cache (le `getOrder` n'a pas d'influence)
* Rajouter un aspect `@AfterReturning` de la méthode `CustomerService.save()`, et là il y a le même soucis.

J'aimerais que les logs passent de ça (certains logs ont été supprimés par soucis de clareté):

```
2018-01-19 00:11:02.594  INFO 8920 --- [           main] hello.Application                        : 1. CUSTOMER -> Customer[id=1, firstName='Jack', lastName='Bauer']
2018-01-19 00:11:02.594 TRACE 8920 --- [           main] .s.t.s.TransactionSynchronizationManager : Initializing transaction synchronization
2018-01-19 00:11:02.594 TRACE 8920 --- [           main] o.s.t.i.TransactionInterceptor           : Getting transaction for [hello.CustomerServiceImpl.save]
2018-01-19 00:11:02.595 TRACE 8920 --- [           main] o.s.t.i.TransactionInterceptor           : Getting transaction for [org.springframework.data.jpa.repository.support.SimpleJpaRepository.save]
2018-01-19 00:11:02.596 DEBUG 8920 --- [           main] org.hibernate.SQL                        : select customer0_.id as id1_0_0_, customer0_.first_name as first_na2_0_0_, customer0_.last_name as last_nam3_0_0_ from customer customer0_ where customer0_.id=?
2018-01-19 00:11:02.597 TRACE 8920 --- [           main] o.s.t.i.TransactionInterceptor           : Completing transaction for [org.springframework.data.jpa.repository.support.SimpleJpaRepository.save]
2018-01-19 00:11:02.597  INFO 8920 --- [           main] hello.SendUpdateAspect                   : AfterReturning aspect code
2018-01-19 00:11:02.597 TRACE 8920 --- [           main] o.s.t.i.TransactionInterceptor           : Completing transaction for [hello.CustomerServiceImpl.save]
2018-01-19 00:11:02.602 DEBUG 8920 --- [           main] org.hibernate.SQL                        : update customer set first_name=?, last_name=? where id=?
2018-01-19 00:11:02.604 TRACE 8920 --- [           main] .s.t.s.TransactionSynchronizationManager : Clearing transaction synchronization
2018-01-19 00:11:02.604  INFO 8920 --- [           main] hello.CustomerServiceImpl                : afterCompletion code
2018-01-19 00:11:02.605 TRACE 8920 --- [           main] o.s.cache.interceptor.CacheInterceptor   : Invalidating cache key [1] for operation Builder[public void hello.CustomerServiceImpl.save(hello.Customer)] caches=[customer] | key='#customer.id' | keyGenerator='' | cacheManager='' | cacheResolver='' | condition='',false,false on method public void hello.CustomerServiceImpl.save(hello.Customer)
2018-01-19 00:11:02.605  INFO 8920 --- [           main] hello.Application                        : 1.1 AFTER SAVE
2018-01-19 00:11:02.605 TRACE 8920 --- [           main] o.s.cache.interceptor.CacheInterceptor   : Computed cache key '1' for operation Builder[public hello.Customer hello.CustomerServiceImpl.findOne(java.lang.Long)] caches=[customer] | key='' | keyGenerator='' | cacheManager='' | cacheResolver='' | condition='' | unless='' | sync='false'
2018-01-19 00:11:02.605 TRACE 8920 --- [           main] o.s.cache.interceptor.CacheInterceptor   : No cache entry for key '1' in cache(s) [customer]
```

à quelque chose comme ça :


```
2018-01-19 00:11:02.594  INFO 8920 --- [           main] hello.Application                        : 1. CUSTOMER -> Customer[id=1, firstName='Jack', lastName='Bauer']
2018-01-19 00:11:02.594 TRACE 8920 --- [           main] .s.t.s.TransactionSynchronizationManager : Initializing transaction synchronization
2018-01-19 00:11:02.594 TRACE 8920 --- [           main] o.s.t.i.TransactionInterceptor           : Getting transaction for [hello.CustomerServiceImpl.save]
2018-01-19 00:11:02.595 TRACE 8920 --- [           main] o.s.t.i.TransactionInterceptor           : Getting transaction for [org.springframework.data.jpa.repository.support.SimpleJpaRepository.save]
2018-01-19 00:11:02.596 DEBUG 8920 --- [           main] org.hibernate.SQL                        : select customer0_.id as id1_0_0_, customer0_.first_name as first_na2_0_0_, customer0_.last_name as last_nam3_0_0_ from customer customer0_ where customer0_.id=?
2018-01-19 00:11:02.597 TRACE 8920 --- [           main] o.s.t.i.TransactionInterceptor           : Completing transaction for [org.springframework.data.jpa.repository.support.SimpleJpaRepository.save]
2018-01-19 00:11:02.597 TRACE 8920 --- [           main] o.s.t.i.TransactionInterceptor           : Completing transaction for [hello.CustomerServiceImpl.save]
2018-01-19 00:11:02.602 DEBUG 8920 --- [           main] org.hibernate.SQL                        : update customer set first_name=?, last_name=? where id=?
2018-01-19 00:11:02.604 TRACE 8920 --- [           main] .s.t.s.TransactionSynchronizationManager : Clearing transaction synchronization
2018-01-19 00:11:02.605 TRACE 8920 --- [           main] o.s.cache.interceptor.CacheInterceptor   : Invalidating cache key [1] for operation Builder[public void hello.CustomerServiceImpl.save(hello.Customer)] caches=[customer] | key='#customer.id' | keyGenerator='' | cacheManager='' | cacheResolver='' | condition='',false,false on method public void hello.CustomerServiceImpl.save(hello.Customer)
2018-01-19 00:11:02.597  INFO 8920 --- [           main] hello.SendUpdateAspect                   : AfterReturning aspect code
2018-01-19 00:11:02.604  INFO 8920 --- [           main] hello.CustomerServiceImpl                : afterCompletion code
2018-01-19 00:11:02.605  INFO 8920 --- [           main] hello.Application                        : 1.1 AFTER SAVE
2018-01-19 00:11:02.605 TRACE 8920 --- [           main] o.s.cache.interceptor.CacheInterceptor   : Computed cache key '1' for operation Builder[public hello.Customer hello.CustomerServiceImpl.findOne(java.lang.Long)] caches=[customer] | key='' | keyGenerator='' | cacheManager='' | cacheResolver='' | condition='' | unless='' | sync='false'
2018-01-19 00:11:02.605 TRACE 8920 --- [           main] o.s.cache.interceptor.CacheInterceptor   : No cache entry for key '1' in cache(s) [customer]
```
```

